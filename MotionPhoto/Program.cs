﻿using Communication;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;

namespace MotionPhoto
{
    class Program
    {
        static void Main(string[] args)
        {
            var options = new Options();
            if (CommandLine.Parser.Default.ParseArguments(args, options))
            {
                if (!Enum.TryParse(options.LogLevel, true, out Serilog.Events.LogEventLevel logLevel))
                {
                    logLevel = Serilog.Events.LogEventLevel.Debug;
                }
                var processPath = AppDomain.CurrentDomain.BaseDirectory;
                var loggerFactory = new LoggerConfiguration()
                    .MinimumLevel.Is(logLevel);
                if (options.Verbose)
                    loggerFactory.WriteTo.ColoredConsole();
                if (options.LogToFile)
                    loggerFactory.WriteTo.RollingFile(pathFormat: Path.Combine(processPath, "logs/log-{Date}.txt"));
                var logger = loggerFactory.CreateLogger();
                logger.Information("Start webcam motion monitor");
                logger.Information($"Upload threshold {options.Threshold}");
                var picasa = new Picasa(processPath, options.SecretsFilename, options.CredentialsDirectory, logger);
                try
                {
                    picasa.Initialize();
                }
                catch (Exception e)
                {
                    logger.Error($"Google credentials are not valid. Message {e.Message}");
                    return;
                }
                var webCam = new WebCam(
                    options.ImageUrl, 
                    options.DLinkLogin, 
                    options.DLinkPassword, 
                    options.Timeout, logger);
                webCam.NewMotion = value =>
                {
                    try
                    {
                        if (!options.DisableUpload)
                        {
                            Task.Run( async () =>
                            {
                                Stream image = null;
                                try
                                {
                                    image = await webCam.ReadImage();
                                }
                                catch(Exception e)
                                {
                                    logger.Error(e, "Fetched camera image failed.");
                                }
                                if(image != null)
                                {
                                    try
                                    {
                                        var now = DateTime.Now.ToString("s");
                                        await picasa.Upload(image, $"Webcam image taken {now} with motion value {value}");
                                    }
                                    catch (Exception e)
                                    {
                                        logger.Error(e, "Uploaded camera image to Google Photo failed.");
                                    }
                                    finally
                                    {
                                        image.Dispose();
                                    }
                                }
                            });
                        }
                    }
                    catch (Exception e)
                    {
                        logger.Error(e, "Fetch/Upload task failed.");
                    }
                };
                if (options.ImageDebug)
                {
                    Stream image = null;
                    try
                    {
                        image = webCam.ReadImage().Result;
                    }
                    catch (Exception e)
                    {
                        logger.Error(e, "Fetched camera image failed.");
                    }
                    if (image != null)
                    {
                        try
                        {
                            picasa.Upload(image, $"Fetched camera test image").Wait();
                        }
                        catch (Exception e)
                        {
                            logger.Error(e, "Uploaded camera image to Google Photo failed.");
                        }
                        finally
                        {
                            image.Dispose();
                        }
                    }
                }
                else
                    while (true)
                    {
                        try
                        {
                            logger.Information("Webcam start listening for motion events.");
                            webCam.Listen(new Uri(options.NotificationUrl), options.Threshold).Wait();
                            logger.Information("Webcam stopped listening for motion events.");
                        }
                        catch (Exception e)
                        {
                            logger.Error(e, "Webcam communcation failed with error.");
                            logger.Information("Restarting...");
                        }
                    }
            }
        }
    }
}
