﻿using CommandLine;
using CommandLine.Text;


namespace TimeLapse
{
    public class Options
    {
        [Option('s', "secrets", Required = false, DefaultValue = null,
          HelpText = "Specify filename with Google credentials.")]
        public string SecretsFilename { get; set; }

        [Option('s', "credentials", Required = false, DefaultValue = null,
          HelpText = "Specify filename with Google credentials.")]
        public string CredentialsDirectory { get; set; }

        [Option('i', "image", Required = false,
         HelpText = "D-Link url to camera for capturing jpeg images (e.g. https://mycamera.com:30000/image/jpeg.cgi).")]
        public string ImageUrl { get; set; }
       
        [Option('l', "login", Required = true,
          HelpText = "D-Link camera login name.")]
        public string DLinkLogin { get; set; }

        [Option('p', "password", Required = true,
          HelpText = "D-Link camea password.")]
        public string DLinkPassword { get; set; }

        [Option('f', "logfile", Required = false,
          HelpText = "Log all messages to files.")]
        public bool LogToFile { get; set; }
        
        [Option('v', "verbose", DefaultValue = false,
          HelpText = "Prints all messages to standard output.")]
        public bool Verbose { get; set; }

        [Option('g', "level", DefaultValue = "Information",
         HelpText = "Prints all messages to standard output.")]
        public string LogLevel { get; set; }

        [Option('t', "time", DefaultValue = 10,
         HelpText = "Time span between uploading images [s].")]
        public int Time { get; set; }

        [Option('k', "timeout", DefaultValue = 100,
         HelpText = "Webcam HTTP request timeout [s].")]
        public int Timeout { get; set; }

        [ParserState]
        public IParserState LastParserState { get; set; }

        [HelpOption]
        public string GetUsage()
        {
            return HelpText.AutoBuild(this,
              (HelpText current) => HelpText.DefaultParsingErrorsHandler(this, current));
        }
    }

}
