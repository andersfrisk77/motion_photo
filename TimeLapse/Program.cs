﻿using Communication;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;

namespace TimeLapse
{
    class Program
    {
        static void Main(string[] args)
        {
            var options = new Options();
            if (CommandLine.Parser.Default.ParseArguments(args, options))
            {
                if (!Enum.TryParse(options.LogLevel, true, out Serilog.Events.LogEventLevel logLevel))
                {
                    logLevel = Serilog.Events.LogEventLevel.Debug;
                }
                var processPath = AppDomain.CurrentDomain.BaseDirectory;
                var loggerFactory = new LoggerConfiguration()
                    .MinimumLevel.Is(logLevel);
                if (options.Verbose)
                    loggerFactory.WriteTo.ColoredConsole();
                if (options.LogToFile)
                    loggerFactory.WriteTo.RollingFile(pathFormat: Path.Combine(processPath, "logs/log-{Date}.txt"));
                var logger = loggerFactory.CreateLogger();
                logger.Information("Start time lapse uploader");
                logger.Information($"Time span {options.Time}");
                var picasa = new Picasa(processPath, options.SecretsFilename, options.CredentialsDirectory, logger);
                try
                {
                    picasa.Initialize();
                }
                catch (Exception e)
                {
                    logger.Error($"Google credentials are not valid. Message {e.Message}");
                    return;
                }
                var webCam = new WebCam(
                    options.ImageUrl,
                    options.DLinkLogin,
                    options.DLinkPassword,
                    options.Timeout, logger);
                var timer = new System.Timers.Timer();
                timer.AutoReset = true;
                timer.Interval = 1000 * options.Time;
                timer.Elapsed += (o, a) =>
                {
                    Task.Run(async () =>
                    {
                        Stream image = null;
                        try
                        {
                            image = await webCam.ReadImage();
                        }
                        catch (Exception e)
                        {
                            logger.Error(e, "Fetched camera image failed.");
                        }
                        if (image != null)
                        {
                            try
                            {
                                var now = DateTime.Now.ToString("s");
                                await picasa.Upload(image, $"Webcam image taken {now}");
                            }
                            catch (Exception e)
                            {
                                logger.Error(e, "Uploaded camera image to Google Photo failed.");
                            }
                            finally
                            {
                                image.Dispose();
                            }
                        }
                    });
                };
                timer.Start();
                System.Threading.Thread.Sleep(-1);
                timer.Stop();
                timer.Dispose();
            }
        }
    }
}
