## MotionPhoto -- Upload motion event images to Google Photo

# Introduction
The aim of this project is to create some simple applications that uses a standard D-Link camera and upload image to Google Photo.
The solution consists of two applications:
 - a motion event lister which sends a fetched image for each retrieved motion event.
 - a time lapse movie maker which uploads images for a specified rate. 

The .NET Core applications built by this project have been tested with a D-Link DCS-2130 and partially tested with D-Link DCS-8000LH.

To access Google Photo for uploading images the application uses the old (but still valid) Picasa API 
(see https://developers.google.com/picasa-web/docs/3.0/developers_guide).

A D-Link camera can be accessed using HTTP and images can easily be fetched (see https://www.ispyconnect.com/man.aspx?n=D-Link).


----------

## MotionPhoto application 

### Features:
    - Access camera on local network or via HTTPS outside local network. 
    - Easy to configure with command line arguments. 
    - Logging to console and rotating files.
    - Adjustible log level.
    - Easy to adjust motion upload sensitivity.
    - Easy to disable uploading of images (for debug and tuning sensitivity). 
    - The application will try to reconnect if camera is unplugged.

The valid command line arguments are:

*  -l/--login			required option is missing.

*  -p/--password			required option is missing.

*  -s, --secrets         (Default: client_secret.json) Specify filename with
                        Google credentials.

*  -s, --credentials     (Default: credentials.json) Specify filename with
                        Google credentials.

*  -n, --notification    D-Link url to camera for capturing jpeg images (e.g.
                        https://mycamera.com:30000/config/notify_stream.cgi).

*  -i, --image           D-Link url to camera for capturing jpeg images (e.g.
                        https://mycamera.com:30000/image/jpeg.cgi).

*  -l, --login           Required. D-Link camera login name.

*  -p, --password        Required. D-Link camea password.

*  -f, --logfile         Log all messages to files.

*  -v, --verbose         (Default: False) Prints all messages to standard
                        output.

*  -g, --level           (Default: Information) Prints all messages to standard
                        output.

*  -e, --image_debug     (Default: False) Upload one image from camera to google
                        photo and then exit application.

*  -t, --threshold       (Default: 95) Camera motion threshold for uploading
                        images.

*  -k, --timeout         (Default: 100) Webcam HTTP request timeout [s].

*  -d, --disable         (Default: False) Listen to motion events but disable
                        upload images to google photo.

*  --help                Display this help screen.

----------

## TimeLapse application 

### Features:
    - Access camera on local network or via HTTPS outside local network. 
    - Easy to configure with command line arguments. 
    - Logging to console and rotating files.
    - Adjustible log level.
    - Easy to time lapse interval.
    - The application will try to reconnect if camera is unplugged.

The valid command line arguments are:

*  -l/--login			required option is missing.

*  -p/--password			required option is missing.

*  -s, --secrets         (Default: client_secret.json) Specify filename with
                        Google credentials.

*  -s, --credentials     (Default: credentials.json) Specify filename with
                        Google credentials.

*  -i, --image           D-Link url to camera for capturing jpeg images (e.g.
                        https://mycamera.com:30000/image/jpeg.cgi).

*  -l, --login           Required. D-Link camera login name.

*  -p, --password        Required. D-Link camea password.

*  -f, --logfile         Log all messages to files.

*  -v, --verbose         (Default: False) Prints all messages to standard
                        output.

*  -g, --level           (Default: Information) Prints all messages to standard
                        output.

*  -t, --time           (Default: 10) Time span between uploading images [s].

*  -k, --timeout         (Default: 100) Webcam HTTP request timeout [s].

*  --help                Display this help screen.

----------


## Prerequisites:

 - Fully configured compatible D-Link IP camera on the same local network or available online.
 - The D-Link IP camera has motion detection configured and activated.
 - Credentials to access Google API using OAuth 2.0.
 - Microsoft Visual Studio 2017 (see https://www.visualstudio.com/vs/community/) or 
  .NET core dotnet-command (see https://www.microsoft.com/net/learn/get-started/windows).   


----------

## Installation Instructions:

It is required the user has created credentials to access the Google account from an application:
See https://developers.google.com/identity/protocols/OAuth2 for more information. Use 
https://console.developers.google.com/apis/credentials to create downloadable credential secrets (choose 
Create client ID (use 'Other' option) and download it.

Open solution file with Microsoft Visual Studio 2017 to build the application or use the dotnet build command.

With dotnet-command do the following:

1. Open a command prompt inside the folder MotionPhoto or TimeLapse.
2. Run 'dotnet build' to build the application.
3. Go to output build folder. Type 'dotnet ./MotionPhoto.dll' to display the help text with commandline flags.
3. Go to output build folder. Type 'dotnet ./TimeLapse.dll' to display the help text with commandline flags.




----------

## Test application:

For testing run the command (note that the file 'client_secret.json' has to be available, for example in the output folder)

1. dotnet MotionPhoto.dll --login login_name --password login_password -n http://camera_ip/config/notify_stream.cgi -i 
   http://camera_ip/dms?nowprofileid=1 -v -g Debug -f -t 50 -d
2. dotnet TimeLapse.dll --login login_name --password login_password -i http://camera_ip/dms?nowprofileid=1 -v -g Debug -f -t 10

----------

## Deploy application to Docker or Raspberry pi

The application can be started a standalone service:  
It is important that the application has been started on a computer with a web browser so the user can accept the interaction with
the application. After accepting the interaction it is important that the folder 'credentials.json' and the file 'client_secret.json' are 
copied to the output folder of the published application. Then the complete folder can be copied to
1. a Docker container (see example of a Dockerfile).
2. a Raspberry pi (use 'dotnet publish -c Release -r linux-arm' to publish).


