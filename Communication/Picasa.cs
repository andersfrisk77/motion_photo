﻿using System;
using System.IO;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using Serilog;
using Serilog.Core;

namespace Communication
{
    public class Picasa
    {
        private static string applicationName = "MotionPhoto";
        private string[] scopes = new string[] { "https://picasaweb.google.com/data/", };
        private Logger logger;

        public string ClientSecretFilename { get; set; } = "client_secret.json";
        private string CredentialsDirectory { get; set; } = "credentials.json";
        public Picasa(
            string baseDirectory, string clientSecretFilename, string credentialsDirectory, Logger logger = null)
        {

            if (clientSecretFilename != null)
                this.ClientSecretFilename = clientSecretFilename;
            else
                this.ClientSecretFilename = Path.Combine(baseDirectory, this.ClientSecretFilename);
            if (credentialsDirectory != null)
                this.CredentialsDirectory = credentialsDirectory;
            else
                this.CredentialsDirectory = Path.Combine(baseDirectory, this.CredentialsDirectory);
            this.logger = logger;
        }
        private UserCredential GetCredential()
        {
            UserCredential credential = null;
            using (var stream =
                new FileStream(this.ClientSecretFilename, FileMode.Open, FileAccess.Read))
            {
                credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                    GoogleClientSecrets.Load(stream).Secrets,
                    scopes,
                    "user",
                    CancellationToken.None,
                    new FileDataStore(this.CredentialsDirectory, true)).Result;
                this.logger?.Verbose("Credential file saved to: " + this.CredentialsDirectory);
            }
            return credential;
        }
        public void Initialize()
        {
            try
            {
                var credentials = this.GetCredential();
            }
            catch(Exception e)
            {
                this.logger?.Error(e, "Cannot get Google Photo credentials");
            }
        }
        public async Task Upload(Stream stream, string title = "webcam")
        {
            string contentType = "image/jpeg";
            UserCredential credentials = null;
            try
            {
                 credentials = this.GetCredential();
            }
            catch (Exception e)
            {
                this.logger?.Error(e, "Cannot get Google Photo credentials");
                return;
            }

            // Create Drive API service.
            var baseClient = new BaseClientService.Initializer()
            {
                HttpClientInitializer = credentials,
                ApplicationName = applicationName,
                HttpClientFactory = new Google.Apis.Http.HttpClientFactory()
            };
            var clientArgs = new Google.Apis.Http.CreateHttpClientArgs()
            {
                ApplicationName = applicationName,
                GZipEnabled = true,
                Initializers = { credentials },
            };
            var factory = baseClient.HttpClientFactory;
            var client = factory.CreateHttpClient(clientArgs);
            using (var content = new StreamContent(stream))
            {
                content.Headers.Add("Content-Type", contentType);
                content.Headers.Add("Slug", title);
                content.Headers.Add("GData-Version", "3");
                var response = await client.PostAsync("https://picasaweb.google.com/data/feed/api/user/default/", content);
                if (!response.IsSuccessStatusCode)
                    this.logger?.Error($"Upload error. Message {response.ReasonPhrase}.");
                else
                    this.logger?.Information($"Upload image with title: {title}");
            }
        }
    }
}
