﻿using Serilog;
using Serilog.Core;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace Communication
{
    public class WebCam
    {
        private double timeout = 10;
        private string imageUrl;
        private string login;
        private string password;
        private Logger logger;
        public Action<int> NewMotion;
        public WebCam(
            string imageUrl,
            string login,
            string password,
            int timeout,
            Logger logger = null)
        {
            this.imageUrl = imageUrl;
            this.login = login;
            this.password = password;
            this.timeout = timeout;
            this.logger = logger;
        }
        public async Task Listen(Uri notificationUrl, int threshold)
        {
            using (var handler = new HttpClientHandler())
            {
                handler.Credentials = new System.Net.NetworkCredential(this.login, this.password);
                handler.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) =>
                {
                    return true;
                };
                using (var client = new HttpClient(handler))
                {
                    client.Timeout = TimeSpan.FromSeconds(this.timeout);
                    var request = new HttpRequestMessage(HttpMethod.Get, notificationUrl);
                    using (var response = await client.SendAsync(
                        request,
                        HttpCompletionOption.ResponseHeadersRead))
                    {
                        var bodyStream = response.Content.ReadAsStreamAsync();
                        if(bodyStream.Wait((int)(1000 * this.timeout)))
                        {
                            using (var body = bodyStream.Result)
                            {
                                using (var reader = new StreamReader(body))
                                {
                                    while (true)
                                    {
                                        var readLineAsync = reader.ReadLineAsync();
                                        if (!readLineAsync.Wait((int)(1000 * this.timeout)))
                                        {
                                            this.logger?.Error("Reader timeout. Stopping longpolling loop.");
                                            break;
                                        }
                                        var read = readLineAsync.Result;
                                        if (!string.IsNullOrEmpty(read))
                                        {
                                            this.logger?.Verbose($"Message {read}");
                                            if (read.Length >= 6 && read.StartsWith("mdv1="))
                                            {
                                                if (int.TryParse(read.Substring(5), out int value))
                                                {
                                                    this.logger?.Debug($"Motion value: {value}");
                                                    if (value >= threshold)
                                                    {
                                                        this.logger?.Information($"Motion change.");
                                                        this.NewMotion?.Invoke(value);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        public async Task<Stream> ReadImage()
        {
            var result = new System.IO.MemoryStream();
            using (var handler = new HttpClientHandler())
            {
                handler.Credentials = new System.Net.NetworkCredential(this.login, this.password);
                handler.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) =>
                {
                    return true;
                };
                using (var client = new HttpClient(handler))
                {
                    client.Timeout = TimeSpan.FromSeconds(this.timeout);
                    var request = new HttpRequestMessage(HttpMethod.Get, this.imageUrl);
                    using (var response = await client.SendAsync(
                        request,
                        HttpCompletionOption.ResponseHeadersRead))
                    {
                        var bodyStream = response.Content.ReadAsStreamAsync();
                        if(bodyStream.Wait((int)(1000 * this.timeout)))
                        {
                            using (var body = bodyStream.Result)
                            {
                                this.logger?.Information($"Read image from motion.");
                                body.CopyTo(result);
                                result.Position = 0;
                            }
                        }
                    }
                }
            }
            return result;
        }
    }
}

